# Vorlesung Web-Engineering 1

Dieses Projekt enthält die Vorlesungsfolien für die Vorlesungen
Web-Engineering 1

an der Dualen Hochschule Baden-Württemberg Karlsruhe.
[karlsruhe.dhbw.de/](https://www.karlsruhe.dhbw.de/startseite.html)

Die Folien sind mit [Reveal.js](https://revealjs.com/) erstellt. Ein einfaches
öffnen der jeweiligen "index.html" stellt die Folien im Browser dar.

Um auch die Code-Beispiel nutzen zu können, müssen die Folien zwingend von
einem Webserver ausgeliefert werden. Dafür ist das npm Projekt vorgesehen:

```bash
$ npm ci
$ npm start
```

Damit sind die Folien unter [localhost:3000](http://localhost:3000) aufrufbar.
Wichtig ist der abschließende "/" am Ende der Url: `http://localhost:3000/added_2021/html/`.

